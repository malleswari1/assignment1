
package com.greatLearning.assignment;

import java.util.*;

public class SuperDepartment {
public String deparmentName() {
 return " Super Department";
}

public String getTodaysWork() {
return "No work as of now ";
}

public String getWorkDeadline() {
return " Nil";
}

public String isTodayAHoliday() {
return "Today is not a holiday";
} 
}


class AdminDepartment extends SuperDepartment{

	public String deparmentName() {
		 return " Admin Department";
		}
	public String getTodaysWork() {
		return " Complete your documents Submission";
		}
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return " Complete by EOD";
		}
		
}


 class HrDepartment extends SuperDepartment{

	public String deparmentName() {
		 return "  HR Department";
		}
		
		public String getTodaysWork() {
		return "Fill todays worksheet and mark your attendance ";
		}
		
		public String getWorkDeadline() {
		return " Complete by EOD";
		}
	 
		public String Activity() {
		return "Team Lunch.";
		} 
}
 class Main {
public static void main(String[] args) {
	while(true){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter your choice");
		System.out.println(" 1 for super department\n"+" 2 for admin department\n"+" 3 for hr department\n"+ "4 for tech department\n"+" 5 for exit");
	int op=sc.nextInt();
	switch(op) {
	case 1:
		SuperDepartment sd = new SuperDepartment();
		  System.out.println(sd.deparmentName());
		  System.out.println(sd.getTodaysWork());
		  System.out.println(sd.getWorkDeadline());
		  System.out.println(sd.isTodayAHoliday());
		  break;
		 case 2 :
			 AdminDepartment ad = new AdminDepartment();
		  System.out.println(ad.deparmentName());
		  System.out.println(ad.getTodaysWork());
		  System.out.println(ad.getWorkDeadline());
		  break;
		 case 3 :
			 HrDepartment hd = new HrDepartment();
		  System.out.println(hd.deparmentName());
		  System.out.println(hd.getTodaysWork());
		  System.out.println(hd.getWorkDeadline());
		  System.out.println(hd.Activity());
		  break;
		 case 4:
			 TechDepartment td = new TechDepartment();
		  System.out.println(td.deparmentName());
		  System.out.println(td.getTodaysWork());
		  System.out.println(td.getWorkDeadline());
		  System.out.println(td.StackInformation());
		  break;
		 case 5:
		  System.exit(1);
		 default:
		  System.out.println("Enter invalid option");
		 
		 }
		 }
	}
	
	
} 


 class TechDepartment extends SuperDepartment{
	public String deparmentName() {
		 return "  Tech Department";
		}
		
		public String getTodaysWork() {
		return "Complete coding of module 1 ";
		}
		
		public String getWorkDeadline() {
		return "  Complete by EOD";
		}
		public String  StackInformation() {
		return "core Java";
		} 
}

